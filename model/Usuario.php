<?php

include_once $caminho . "/model/Banco.php"; 

class Usuario extends Banco{
	
	private $nome;
	private $sobrenome;
	private $email;
	private $senha;
	private $foto;

	private $bd;
	private $tabela;

	
	public function __construct()
	{
		$this->bd = new Banco();
		$this->tabela = "usuarios";
	}


	public function listar()
	{
		$query = "SELECT * FROM $this->tabela";        
        $resultado = $this->bd->pdo->prepare($query);
        $resultado->execute();
        return $resultado->fetchAll();
	}


	public function mostrar($id)
	{
		$query = "SELECT * FROM $this->tabela WHERE id = $id";
		$resultado = $this->bd->pdo->prepare($query);
        $resultado->execute();        
		if( $resultado->columnCount() == 5 ){
            return $resultado->fetch(PDO::FETCH_ASSOC);
		}
		else{
			return null;
		}
	}

	


	public function salvar(){
		$nome 		= $this->nome;
		$sobrenome 	= $this->sobrenome;
		$email 		= $this->email;
		$senha 		= $this->senha;

		$query = "INSERT INTO $this->tabela (nome, sobrenome, email, senha) VALUES (:nome, :sobrenome, :email, :senha)";
        $resultado = $this->bd->pdo->prepare($query);
        $resultado->bindParam(':nome', $nome);
        $resultado->bindParam(':sobrenome', $sobrenome);
        $resultado->bindParam(':email', $email);
        $resultado->bindParam(':senha', $senha);

		if( $resultado->execute() ){
			return true;
		}
		else{
			return false;
		}
	}


	public function atualizar($id){
		$nome      = $this->nome;
        $sobrenome = $this->sobrenome;
        $email     = $this->email;
        $senha     = $this->senha;

		$query = "UPDATE $this->tabela SET nome=:nome, sobrenome=:sobrenome, email=:email, senha=:senha WHERE id=$id";
        $resultado = $this->bd->pdo->prepare($query);
        $resultado->bindParam(':nome', $nome);
        $resultado->bindParam(':sobrenome', $sobrenome);
        $resultado->bindParam(':email', $email);
        $resultado->bindParam(':senha', $senha);
		if( $resultado->execute() ){
			return true;
		}
		else{
			return false;
		}
	}


	public function excluir($id){
		$query = "DELETE FROM $this->tabela WHERE id = $id";
        $resultado = $this->bd->pdo->prepare($query);
		if( $resultado->execute() ){
			return true;
		}
		else{
			return false;
		}
	}


	

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSobrenome()
    {
        return $this->sobrenome;
    }

    /**
     * @param mixed $sobrenome
     *
     * @return self
     */
    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = $sobrenome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     *
     * @return self
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     *
     * @return self
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }
}

