<?php
	
	class Banco{
		protected $pdo = "";

		private $host 	= "localhost";
		private $usuario = "root";
		private $senha 	= "";
		private $banco 	= "phpoo_bd";

		public function __construct() {

			try {
				$this->pdo = new PDO("mysql:host={$this->host}; dbname={$this->banco}", $this->usuario, $this->senha);
				$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			} catch (PDOException  $e) {
				print $e->getMessage();
			}

		}

	}

?>