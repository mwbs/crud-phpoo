<?php
	/* Redireciona para a raiz do sistema */
	$caminho = "../";
	$mensagen = "";
	$tipo = "";

	require_once $caminho . "model/Usuario.php";

	$usuario = new Usuario();

	if( isset( $_GET['tag']) )
	{
		$tag = base64_decode($_GET['tag']);
		if($tag == 'cadastrar')
		{
			include $caminho . "view/usuario/cadastrar.php";
			exit;
		}
		else if($tag == 'editar')
		{
			$id = base64_decode($_GET['cod']);
			$registro = $usuario->mostrar($id);
			//print_r($registro); die;
			include $caminho . "view/usuario/editar.php";
			exit;
		}
		else if($tag == 'deletar')
		{
			$id = base64_decode($_GET['cod']);
			if( $usuario->excluir($id) ){
				$mensagem = "<strong>Sucesso!</strong> O usuário foi excluido.";
				$tipo = "alert-success"; // Sucesso
			}
			else{
				$mensagem = "<strong>Erro!</strong> Não foi possivel excluir o usuário.";
				$tipo = "alert-danger"; // Erro
			}
		}
	}

		
	if( isset($_POST['hash']) )
	{
		/* Pega a hash */
		$hash = $_POST['hash'];

		if($hash == sha1('salvar'))
		{
			$nome = $_POST['nome'];
			$sobrenome = $_POST['sobrenome'];
			$email = $_POST['email'];
			$senha = sha1( $_POST['senha'] ); 
			
			/* Encapsula o Objeto */
			$usuario->setNome($nome);
			$usuario->setSobrenome($sobrenome);
			$usuario->setEmail($email);
			$usuario->setSenha($senha);
			if( $usuario->salvar() ){
				$mensagem = "<strong>Sucesso!</strong> O usuário foi cadastrado.";
				$tipo = "alert-success"; // Sucesso
			}
			else{
				$mensagem = "<strong>Erro!</strong> Não foi possivel cadastrar o usuário.";
				$tipo = "alert-danger"; // Erro
			}
		}
		else if($hash == sha1('atualizar'))
		{
			$id = base64_decode( $_POST['cod'] ); // O id para atualizar
			$nome = $_POST['nome'];
			$sobrenome = $_POST['sobrenome'];
			$email = $_POST['email'];
			$senha = sha1( $_POST['senha'] ); 
			
			/* Encapsula o Objeto */
			$usuario->setNome($nome);
			$usuario->setSobrenome($sobrenome);
			$usuario->setEmail($email);
			$usuario->setSenha($senha);

			if( $usuario->atualizar($id) ){
				$mensagem = "<strong>Sucesso!</strong> O usuário foi atualizado.";
				$tipo = "alert-success"; // Sucesso
			}
			else{
				$mensagem = "<strong>Erro!</strong> Não foi possivel atualizar o usuário.";
				$tipo = "alert-danger"; // Erro
			}
		}

	}
	
	$registros = $usuario->listar();
	include $caminho . "./view/usuario/index.php";

	

?>