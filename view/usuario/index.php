<?php
	include $caminho . "view/topo.php";
?>
	<?php if ( !empty($mensagem) ): ?>
		<div class="alert <?php echo $tipo; ?> alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo $mensagem; ?>
		</div>
	<?php endif ?>

	<div class="row">
		<div class="container-fluid">
			<h3>Usuários</h3>
			<p>Área destinada para os usuários cadastrados no sistema</p>
		</div>	
	</div>
	<div class="row">	
		<div class="col-sm-3">	
			<a href="<?php echo $caminho . 'controller/UsuarioController.php?tag=' .base64_encode('cadastrar'); ?>" class="btn btn-primary text-center">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"> </span>
				Novo usuário
			</a>
		</div>		
	</div>
	<div class="row">
		<div class="col-sm-12">
			<br>
			<div class="well">				

				<table class="table table-hover">
					<thead>
						<tr>
							<th>Opções</th>
							<th>Nome</th>
							<th>Sobrenome</th>
							<th>Email</th>
							<th>Senha</th>
						</tr>
					</thead>
					<tbody>
						<?php if( empty($registros) ){ ?>
							<tr>
								<td rowspan="5"> Não há usuários </td>
							</tr>

						<?php } else { ?>

							<?php foreach($registros as $registro): ?>
								<tr>
									<td>
										<a class="btn btn-success" href="<?php echo $caminho.'controller/UsuarioController.php?tag='.base64_encode('editar').'&amp;cod='. base64_encode($registro['id']); ?>" role="button">
											<span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span>
										</a>
										<a class="btn btn-danger" href="<?php echo $caminho.'controller/UsuarioController.php?tag='.base64_encode('deletar').'&amp;cod='. base64_encode($registro['id']); ?>" role="button">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"> </span>
										</a>
									</td>
									<td> <?php echo $registro['nome']; ?> </td>
									<td> <?php echo $registro['sobrenome']; ?> </td>
									<td> <?php echo $registro['email']; ?> </td>
									<td> *** </td>
								</tr>
							<?php endforeach ?>

						<?php } ?>
						
						
					</tbody>
				</table>
			</div>
		</div>
	</div>

<?php include $caminho . "view/rodape.php"; ?>
