<?php
	include $caminho . "view/topo.php";
?>

	<div class="row">
		<div class="container-fluid">
			<h3>Usuários</h3>
			<p>Área destinada para os usuários cadastrados no sistema</p>
		</div>	
	</div>
	<div class="row">	
		<div class="col-sm-3">	
			<a href="<?php echo $caminho.'HomeController.php?tag=cadastrar'; ?>" class="btn btn-primary text-center">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"> </span>
				Novo usuário
			</a>
		</div>
		
	</div>
	<div class="row">
		<div class="col-sm-12">
			<br>
			<div class="well">

				<form action="<?php echo $caminho . 'controller/UsuarioController.php'; ?>" method="POST">
					<div class="form-group">
						<label for="nome">Nome:</label>
						<input type="text" name="nome" class="form-control" id="nome">
					</div>
					<div class="form-group">
						<label for="sobrenome">Sobrenome:</label>
						<input type="text" name="sobrenome" class="form-control" id="sobrenome">
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="email" name="email" class="form-control" id="email">
					</div>
					<div class="form-group">
						<label for="senha">Senha:</label>
						<input type="password" name="senha" class="form-control" id="senha">
					</div>

					<input type="hidden" name="hash" value="<?php echo sha1('salvar'); ?>">
					
					<button type="submit" class="btn btn-success">Cadastrar</button>
				</form>

			</div>
		</div>
	</div>

<?php include $caminho . "view/rodape.php"; ?>
