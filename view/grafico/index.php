<?php
	include $caminho . "view/topo.php";
?>
	<?php if ( !empty($mensagem) ): ?>
		<div class="alert <?php echo $tipo; ?> alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo $mensagem; ?>
		</div>
	<?php endif ?>

	<div class="row">
		<div class="container-fluid">
			<h3>Gráficos</h3>
			<p>Área destinada para apresentação de gráficos</p>
		</div>	
	</div>
	<div class="row">	
		<div class="col-sm-3">	
			<a href="<?php echo $caminho . 'controller/UsuarioController.php?tag=' .base64_encode('cadastrar'); ?>" class="btn btn-primary text-center">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"> </span>
				Novo usuário
			</a>
		</div>		
	</div>
	<div class="row">
		<div class="col-sm-12">
			<br>
			<div class="well">
				
				<!-- Inicia os gráficos -->
				<div class="row">

					<div class="col-sm-6">

						<canvas id="grafico" width="200" height="200"></canvas>

					</div>
										
				</div>
				
			</div>
		</div>
	</div>

<?php include $caminho . "view/rodape.php"; ?>
