<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Painel php</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $caminho . 'assets/css/style.css'; ?>">

  
</head>
<body>

  <nav class="navbar navbar-inverse visible-xs">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">PHP</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li class=""><a href="#">Usuários</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row content">
      <div class="col-sm-3 sidenav hidden-xs">
        <h2>PHP</h2>
        <ul class="nav nav-pills nav-stacked">
          <li class="">
            <a href="<?php echo $caminho.'controller/UsuarioController.php'; ?>">Usuários</a>
          </li>
          <li class="">
            <a href="<?php echo $caminho.'controller/GraficoController.php'; ?>">Gráficos</a>
          </li>
        </ul><br>
      </div>
      <br>

      <div class="col-sm-9">
      

